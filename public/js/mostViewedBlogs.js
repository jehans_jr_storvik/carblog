$(document).ready(function () {
    $.ajax
    ({
        url: '/getMostViewedBlogs',
        dataType: 'json',
        success: function(data)
        {
            for(var i=0; i < data.length; i++) {
                console.log(data[i]);
                $("#mostViewedBlogs").append(data[i].views + " - <a href='" + data[i].completeUrl + "'>"+ data[i].url +"</a></br>" );
            }
        },
        error: function(request, status, error) {
            alert(request.responseText);
        }
    });
});