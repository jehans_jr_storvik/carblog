$(document).ready(function () {
    //Do not load unless used is logged in
    if($("#ownedBlogs").length > 0) {
    $.ajax
    ({
        url: '/getOwnedBlogs/' + Math.random(),
        dataType: 'json',
        success: function(data)
        {
            console.log(data);
            if(data.length == 0 || data == null) {
                console.log("Ingen blogger");
                $("#ownedBlogs").append('<li><a href="/newBlog">Create new blog<a></li>');
                $("#ownedBlogs2").append('<li><a href="/newBlog">Create new blog<a></li>');
            }
            for(var i=0; i < data.length; i++) {
                
                $("#ownedBlogs").append('<li><a href="'+ data[i].completeUrl +'">'+ data[i].url +'</a></li>');
                $("#ownedBlogs2").append('<li><a href="'+ data[i].completeUrl +'">'+ data[i].url +'</a></li>');
            }

        },
        error: function(request, status, error) {
           console.log(request.responseText);
        }
    });
    }
});
