$(document).ready(function () {  
    
    $('#name').keyup(function (e) {
            /*var element = document.getElementById('name');
            var oldval = element.value;
            
            element.value = element.value.replace(/[^a-zA-Z0-9]+/, '');*/
            
    });
    
    $('#name').focusin(function (e) {
        $('#nameExists').hide();
    });
    
    $('#name').focusout(function (e) {  
        checkUsername();
    });
    
    $('#confirm-password').keyup(function (e) {
        checkPassword();
    });
    
    $('#password-form').keyup(function (e) {
        checkPassword();
    });
});

function checkPassword() {
    //KUN LOCKING
        var password = $('#password-form').val();
        var confirm = $('#confirm-password').val();
        if(password === confirm) {
            $('#helper').hide();
            validate();
        } else{
            $('#submitNewUser').prop('disabled', true);
            $('#helper').show();
        }
}

function validate() {
    //OPEN ONLY
            if ($('#nameExists').is(':visible') || $('#helper').is(':visible')) {
                //$('#submitNewUser').prop('disabled', true);
            } else {
                $('#submitNewUser').prop('disabled', false);
            }
}

function checkUsername() {
    var name = $("#name").val();
        $.ajax
        ({
            url: '/checkUsername',
            data: {
                    'userName' : name,
            },
            dataType: 'json',
            success: function(data)
            {
               if(data == 1) {
                   $('#nameExists').show();
                   $('#submitNewUser').prop('disabled', true);
               } else {
                   $('#nameExists').hide();
               }
            }
        });
        validate();
}


