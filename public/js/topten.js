$(document).ready(function () { 
    $("select[name='carmakes_makes_id']").change(function() {
        searchCarmake();
    });

});
var typingTimer;                //timer identifier
var doneTypingInterval = 450;  //time in ms, 5 second for example
var $input = $('#search');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown 
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

//user is "finished typing," do something
function doneTyping () {
    searchRandom();
}
function searchRandom() {
        var search = $("#search").val();
        $.ajax
    ({
        url: '/search',
        data: {
        	'search' : search,
        },
        dataType: 'json',
        success: function(data)
        {
            console.log(data);
            $("#sidebarList").empty();
            if(data.length == 0) {
                $("#sidebarList").append("<p>No blogs found</p>");
            }
            for(var i=0; i < data.length; i++) {
                $("#sidebarList").append("<h4><a href='"+ data[i].completeUrl+ "'>" +data[i].url +  "</br><img src='"+data[i].logo+"'></img></a></h4>");
            }
        },
        error: function(request, status, error) {
            alert(request.responseText);
        }
    });
    }
    
function searchCarmake() {
        var carMakeId = $("#carmakes").val();
        console.log(carMakeId);
        $.ajax
    ({
        url: '/gettoptencarmake',
        data: {
        	'carmakeid' : carMakeId,
        },
        dataType: 'json',
        success: function(data)
        {
            console.log(data);
            $("#sidebarList").empty();
            if(data.length == 0) {
                $("#sidebarList").append("<p>No blogs found</p>");
            }
            for(var i=0; i < data.length; i++) {
                $("#sidebarList").append("<h4><a href='"+ data[i].completeUrl+ "'>" +data[i].url +  "</br><img src='"+data[i].logo+"'></img></a></h4>");
            }
        },
        error: function(request, status, error) {
            alert(request.responseText);
        }
    });
    }