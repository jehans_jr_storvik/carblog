<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\URL;
use App\Models\Blog as Blog;
use App\Models\Carmakes as Carmakes;
use App\Models\Usertags as Usertags;
use App\Models\Carmodels as Carmodels;
use App\Models\Toolbox as Toolbox;
use App\Models\Subscribe as Subscribe;
use App\User as User;
use DB;

use Auth;

class BlogController extends Controller
{
    //use DispatchesJobs, ValidatesRequests;
    public function showBlog(Request $request) { 
        $blog = Toolbox::getCorrectItem($request);
        Blog::addViewCount($blog->id);
        $blogPosts = Blog::getBlogPosts($blog->id);
        $edit = BlogController::allowedToEdit($blog->user);
        $subscribes = Subscribe::subscribesAlready($blog->id);
        $tags = Usertags::getAllTagsForBlog($blog->id);
        return view('pages.viewBlog', [
            'edit' => $edit,
            'tags' => $tags,
            'url' => $request->url(),
            'make' => Carmakes::where('makes_id', '=', $blog->carmakeid)->first()->makes_name,
            'model' => Carmodels::where('models_id', '=', $blog->carmodelid)->first()->models_name,
            'blog' => $blog,
            'subscribes' => $subscribes,
            'blogPosts' => $blogPosts]); 
    }
    

    public function search() {
        return null;
    }
    public function gettoptencarmake(Request $request) {
        $carmakeid = $request->input('carmakeid');
        $blogs = DB::table('blog')
                ->select('id', 'logo','url')
                ->where('carmakeid', '=', $carmakeid)
                ->get();
        foreach($blogs as $blog) {
            $blog->completeUrl = Blog::getUrl($blog->id);
            unset($blog->id);
        }
        return json_encode($blogs);
    }
    public function subscribe(Request $request) {
        Subscribe::subscribeToBlog($request->input('blogId'));
        return "true";
    }
    
    public function showProfile(Request $request) {
        /*if ($request->isMethod('post')) 
        {
            
        }*/
        $user = User::find(Auth::user()->id);
        $blogs = Blog::getAllBlogsForUser(Auth::user()->id);
        foreach($blogs as $blog) {
            $blog->completeUrl = Blog::getUrl($blog->id);
        }
        return view('pages.showProfile', [
            'blogs' => $blogs,
            'user' => $user
        ]);
    }

    public function routeHelper(Request $request) {
        return URL::back();
        return "123";
    }
    private function allowedToEdit($blogUserId) {
        if(Auth::check()) {
            if(Auth::user()->id == $blogUserId) {
                return true;
            }
        }
        return false;
    }
    
    public function editBlog(Request $request) {
        $success = "";
        if ($request->isMethod('post')) {
            Blog::editBlog($request);
            $success = "Successfully updated blog!";
        }
        
        $blog = Toolbox::getCorrectItem($request);
        $tags = Usertags::getAllTagsForBlog($blog->id);
        $tagsString = "";
        foreach($tags as $tag) {
            $tagsString = $tagsString . $tag->tag . ", ";
        }
                
        return view('pages.editBlog', [
            'url' => $request->url . Auth::user()->name,
            'carmakes' => Carmakes::lists('makes_name', 'makes_id'),
            'carmodels' => Carmodels::where('makes_id' , '=', 1)->lists('models_name', 'models_id'),
            'blog' => $blog,
            'tags' => $tagsString,
            'success' => $success
            ]);
    }
    
    public function newBlog(Request $request) {
        if ($request->isMethod('post')) {
            $blogId = Blog::createNewblog($request);
            if($blogId > 0) {
                $url = Blog::getUrl($blogId);
                return redirect($url);
            } else {
                return "You already own a blog with that name";
            }
        }
        
        return view('pages.newBlog', [
            'url' => $request->url . Auth::user()->name,
            'carmakes' => Carmakes::lists('makes_name', 'makes_id'),
            'carmodels' => Carmodels::where('makes_id' , '=', 1)->lists('models_name', 'models_id'),
            ]);
    }
    
    public function getMostViewedBlogs() {
        $blogs = Blog::getMostViewedBlogs();
        foreach($blogs as $blog) {
            $blog->completeUrl = Blog::getUrl($blog->id);
        }
        
        return $blogs;
    }
    
    public function getOwnedBlogs($rnd) {
        $ownedBlogs = Blog::getOwnedBlogs();
        foreach($ownedBlogs as $blog) {
            $blog->completeUrl = Blog::getUrl($blog->id);
        }
        return $ownedBlogs;
    }
}
