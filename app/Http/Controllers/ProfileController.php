<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use Illuminate\Http\Request;
use App\Models\Toolbox as Toolbox;

use Auth;
use App\Models\Blog as Blog;

class ProfileController extends Controller
{
    //use DispatchesJobs, ValidatesRequests;
    public function showProfile(Request $request) {
        $user = Toolbox::getCorrectItem($request);
        $errors = array();
        $edit = false;
        if(Auth::check() && ($user->id == Auth::user()->id)) {
            $edit = true;
            if ($request->isMethod('post')) 
            {
                if($request->input('password_confirmation') == $request->input('new_password'))
                {
                    if(Hash::check($request->input('current_password'), Auth::user()->password)) {
                        $user = Auth::user();
                        $user->password = Hash::make($request->input('new_password'));
                        $user->save();
                        
                        $errors[] = "Changed password successfully!";
                    } else {
                        $errors[] = "Current password did not match your actual password";
                    }
                }
                else {
                    $errors[] = "'New password', and 'Confirm new password' did not match";
                }
            }
        } 
        $blogs = Blog::getAllBlogsForUser(Auth::user()->id);
        
        foreach($blogs as $blog) {
            $blog->completeUrl = Blog::getUrl($blog->id);
            $blog->owner = $user->name;
        }
        
        return view('pages.viewProfile', ['user' => $user,
                                         'blogs' => $blogs,
                                         'edit' => $edit,
                                         'errors' => $errors]); 
    }
    
    public function checkUsername(Request $request) {
        $username = $request->input("userName");
        $user = DB::table('users')
                ->where('name', '=', $username)
                ->get();
        if($user == null) {
            return 0;
        }
        return 1;
    }
    
}