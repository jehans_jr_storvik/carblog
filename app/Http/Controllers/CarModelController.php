<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carmodels as Carmodels;


class CarModelController extends Controller   {
    public function getCarModels(Request $request) {
        //Sjekk om input inneholder noe annet enn tall?
        $carMakeId = $request->input("carMakeId");
        $carModels = Carmodels::getAllModelsForMake($carMakeId);
        return $carModels;
    }
}