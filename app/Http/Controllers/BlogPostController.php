<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Collective\Html\FormFacade;
use Illuminate\Http\Request;

use App\Models\Blog as Blog;
use App\Models\Comments as Comments;

use App\Models\BlogPost as BlogPost;
use App\Models\Toolbox as Toolbox;
use Auth;
use Image;
class BlogPostController extends Controller   {
    public function newBlogPost(Request $request) {
        if ($request->isMethod('post')) {
            BlogPost::createNewBlogPost($request);
            $url = str_replace("/newBlogPost", "", $request->url());
            return redirect($url);
        }
        return view('pages.newBlogPost');
    }
    
    public function viewBlogPost(Request $request) {
        $blogPost = Toolbox::getCorrectItem($request);
        if ($request->isMethod('post')) {
            if(strlen($request->input('comment')) > 1) 
            {
                $comment = new Comments;
                $comment->userid = Auth::user()->id;
                $comment->text = $request->input('comment');
                $comment->blogpostid = $blogPost->id;
                $comment->save();
            }
        }
        BlogPost::addViewCount($blogPost->id);  
        return view('pages.viewBlogPost', ['blogpost' => $blogPost,
            'url' => $request->url(),
            'comments' => Comments::getComments($blogPost->id)
                ]);
    }    
}

function GUID()
{
    if (function_exists('com_create_guid') === true)
    {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}