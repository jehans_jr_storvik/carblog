<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Auth;
use User;
use App\Models\Blog as Blog;
use App\Models\BlogPost as BlogPost;
use App\Models\Carmakes as Carmakes;


class HomeController extends Controller
{
    //use DispatchesJobs, ValidatesRequests;
    public function index() {
        $blogposts = BlogPost::getLatestBlogPosts();
        
        foreach($blogposts as $blogpost) {
            $blogpost->completeUrl = Blogpost::generateUrlFromId($blogpost->id);
            $blogpost->owner = Blogpost::getOwner($blogpost->id);
           /* $cnt = $blogpost->content;
            preg_match ('/<img.*<\/div>/', $blogpost->content, $matches, PREG_OFFSET_CAPTURE);
            $blogpost->content = substr($cnt, 0,$matches[0][1]+54);*/
        }
        
        $blogs = Blog::getMostViewedBlogs();
        foreach($blogs as $blog) {
            $blog->completeUrl = Blog::getUrl($blog->id);
            $blog->owner = Blog::getOwnerOfBlog($blog->user)->name;
        }
        
        
        return view('pages.home', ['blogposts' => $blogposts,
            'carmakes' => Carmakes::lists('makes_name', 'makes_id'),
            'blogs' => $blogs]); 
    }
    
    public function indexCopy() {
        
        $blogs = Blog::getMostViewedBlogs();
        
        foreach($blogs as $blog) {
            $blog->completeUrl = Blog::getUrl($blog->id);
            $blog->owner = Blog::getOwnerOfBlog($blog->user)->name;
        }
        return view('pages.home', ['blogs' => $blogs]); 
    }
    
    public function test() {
        echo "test";
    }
    
    function strpos_array($haystack, $needles) {
    if ( is_array($needles) ) {
        foreach ($needles as $str) {
            if ( is_array($str) ) {
                $pos = strpos_array($haystack, $str);
            } else {
                $pos = strpos($haystack, $str);
            }
            if ($pos !== FALSE) {
                return $pos;
            }
        }
    } else {
        return strpos($haystack, $needles);
    }
}
    
}