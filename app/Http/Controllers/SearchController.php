<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\URL;
use App\Models\Blog as Blog;
use App\Models\Tags as Tags;

use App\Models\Toolbox as Toolbox;
use App\User as User;
use Auth;

class SearchController extends Controller
{
    public function search(Request $request) {
        $blogs = array();
        $words = explode(" ",$request->input('search'));
        if(strlen($request->input('search')) > 2)
        {
            foreach($words as $search) 
            {
                $blogs = Blog::
                    where('url', 'LIKE', '%'.$search . '%')
                    ->orWhere('engine', 'LIKE', '%'.$search . '%')
                    ->orWhere('year', 'LIKE', '%'.$search . '%')
                    ->get();

                $tags = Tags::
                        where('tags.tag', 'LIKE', '%' . $search . '%')
                        ->join('usertags', 'tags.id','=','usertags.tagid')
                        ->join('blog', 'usertags.blogid','=', 'blog.id')
                        ->get();

                foreach($tags as $tag) {
                    $blog = new Blog;
                    $blog->id = $tag['blogid'];
                    $blog->url = $tag['url'];
                    $blog->completeUrl = Blog::getUrl($tag['blogid']);
                    $blog->logo = $tag['logo'];
                    $blogs[] = $blog;
                }
            }
        }
        else {
            $blogs = Blog::getMostViewedBlogs();
            foreach($blogs as $blog) {
                $blog->completeUrl = Blog::getUrl($blog->id);
                $blog->owner = Blog::getOwnerOfBlog($blog->user)->name;
            }
        }
        
        return json_encode($blogs);
        //blog url
       //Bruker name
        //bloggpost tittel
    }
    
    public function searchUrl($url) {
        
    }
}