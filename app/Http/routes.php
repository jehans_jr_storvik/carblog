<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get("/test", 'HomeController@test');
Route::get('/', 'HomeController@index');


//BLOGCONTROLLER
Route::get('/newBlog', 'BlogController@newBlog');
Route::post('/newBlog', 'BlogController@newBlog');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//AJAX
Route::get('/gettoptencarmake', 'BlogController@gettoptencarmake');
Route::get('/getOwnedBlogs/{rnd}', 'BlogController@getOwnedBlogs');
Route::get('/getMostViewedBlogs', 'BlogController@getMostViewedBlogs');
Route::get('/getCarModels', 'CarModelController@getCarModels');
Route::get('/routeHelper', "BlogController@routeHelper");
Route::get('/checkUsername', "ProfileController@checkUsername");
Route::get('/search', "SearchController@search");
Route::get('/subscribe', 'BlogController@subscribe');
//Route::post('/uploadPhotos', 'BlogPostController@uploadPhotos');

Route::any('/uploadPhotos2', 'UploadController@upload');
// Authentication routes...
Route::get('/auth/login', 'Auth\AuthController@getLogin');
Route::post('/auth/login', 'Auth\AuthController@postLogin');
Route::get('/auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('/auth/register', 'Auth\AuthController@getRegister');
Route::post('/auth/register', 'Auth\AuthController@postRegister');
////////////////////////////////////////////////////////////////////////////////////

//Offentlig profile
Route::get('/{user}', 'ProfileController@showProfile');
Route::post('/{user}', 'ProfileController@showProfile');

Route::get('/{user}/{url}', 'BlogController@showBlog');

Route::any('/{user}/{url}/newBlogPost', 'BlogPostController@newBlogPost');

Route::get('/{user}/{url}/editBlog',  'BlogController@editBlog');
Route::post('/{user}/{url}/editBlog', 'BlogController@editBlog');

Route::any('/{user}/{url}/{title}', 'BlogPostController@viewBlogPost');

