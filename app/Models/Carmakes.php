<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Carmakes extends Model
{
    protected $table = 'carmakes';
    public $timestamps = false;
    
    public static function getAll() {
        $carmakes = DB::table('carmakes')
                ->orderBy('id','desc')
                ->get();
        return $carmakes;
    }
}