<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Carmodels extends Model
{
    protected $table = 'carmodels';
    public $timestamps = false;
    
    public static function getAllModelsForMake($carMakeId) {
        $carmodels = DB::table('carmodels')
                ->where('makes_id', '=', $carMakeId)
                ->get();
        return $carmodels;
    }
    
    public static function getForAcura() {
        $carmodels = DB::table('carmodels')
                ->where('makes_id', '=', 1)
                ->get();
        return $carmodels;
    }
}