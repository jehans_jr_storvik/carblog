<?php
namespace App\Models;
use DB;
use Auth;
use App\Models\Toolbox;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\User as User;

class Blog extends Model
{
    protected $table = 'blog';
    protected $appends = array('completeUrl', 'owner');
    public $timestamps = false;
    
    public static function getBlogs() {
        $blogs = DB::table('blog')
                ->join('users', 'blog.user', '=','users.id')
                ->get();
        return $blogs;
    }
    
    public static function getOwnerAttribute() {
        return "";
    }
    public static function getCompleteUrlAttribute() {
        return "";
    }
    public static function getOwnerOfBlog($userId) {
        $user = DB::table('users')
                ->where('users.id', '=', $userId)
                ->get();
        return $user[0];
        
    }
    public static function getOwnedBlogs() {
        $ownedBlogs = DB::table('blog')
                ->where('user', '=', Auth::user()->id)
                ->get();
        return $ownedBlogs;
    }
    public static function getAllBlogsForUser($userId) {
        $ownedBlogs = DB::table('blog')
                ->where('user', '=', $userId)
                ->get();
        return $ownedBlogs;
    }
    public static function getBlogPosts($blogId) {
        $blogPosts = DB::select('SELECT * FROM blogposts WHERE blogid = ?'
                . ' ORDER BY id DESC', [$blogId]);
        return $blogPosts;
    }
    
    public static function getMostViewedBlogs() {
        $data = DB::table('blog')
                ->orderBy('views', 'desc')
                ->take(16)
                ->get();
        return $data;
    }

    public static function getUrl($blogId) {
        $data = DB::table('blog')
                ->join('users', 'blog.user', '=','users.id')
                ->where('blog.id', '=', $blogId)
                ->get();
        $url = "/" . $data[0]->name . "/" . $data[0]->url;
        return $url;
    }
    public static function addViewCount($blogId) {
        DB::table('blog')
                ->where('id', $blogId)
                ->increment('views');
    }
    public static function getBlogId($blogUrl, $userId) {
        $data = DB::table('blog')
                ->select('id')
                ->where('user', '=', $userId)
                ->where('url', '=', $blogUrl)
                ->get();
        if(isset($data[0]->id)) {
            return $data[0]->id;
        }
        return null;
    }
    
    public static function createNewBlog(Request $request) {
        $url = $request->input('url');
        $exists = Blog::where('url', '=', $url)
                ->where('user', '=', Auth::user()->id)
                ->first();
        if($exists == null) 
        {
            $blogId = DB::table('blog')->insertGetId(
                [
                    'url' => $url,
                    'carmakeid' => $request->input('carmakes_makes_id'),
                    'carmodelid' => $request->input('carmodels_models_id'),
                    'year' => $request->input('Year'),
                    ///'color' => $request->input('Color'),
                    'color' => "NaN",
                    'user' => Auth::user()->id
                ]
                );
        }
        else 
        {
            $blogId = 0;
        }
        return $blogId;
    }
   
    public static function editBlog($request) {
        $blog = Toolbox::getCorrectItem($request);
        DB::table('blog')
                ->where('id','=', $blog->id)
                ->update([
                    'logo' => $request->input('logo'),
                    'carmakeid' => $request->input('carmakes_makes_id'),
                    'carmodelid' => $request->input('carmodels_models_id'),
                    'engine' => $request->input('engine'),
                    'Year' => $request->input('Year'),
                    'About' => $request->input('about')
                    ]);
        $tags = str_replace(" ", "," ,$request->input('tags'));
        $tagsExploded = explode(",", $tags);
        if(count($tagsExploded) > 0) {
            Usertags::where('blogid', '=', $blog->id)
                    ->delete();
        }
        foreach($tagsExploded as $tag) {
            
            if($tag != "") {
                Usertags::insertUsertag($tag, $blog->id);
            }
        }

        
    }
    public static function getBlogUrlFromRequest($request) {
        $url = $request->url();
        $url = explode("/", $url);
        $blogUrl = $url[count($url)-1];
        return $blogUrl;
}
}
