<?php
namespace App\Models;
use DB;
use Auth;
use App\Models\Toolbox;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\User as User;

class Subscribe extends Model
{
    protected $table = 'subscribe';
    //protected $appends = array('completeUrl', 'owner');
    public $timestamps = false;
    
    public static function subscribeToBlog($blogid) {
        if(!self::subscribesAlready($blogid)) {
            $data = array(
                'blogid' => $blogid,
                'userid' => Auth::user()->id
            );
            Subscribe::insert($data);
        } else {
            Subscribe::where('blogid', '=', $blogid)
                    ->where('userid', '=', Auth::user()->id)
                    ->delete();
        }
    }
    
    public static function subscribesAlready($blogid) {
        if(Auth::check() == false) {
            return false;
        }
        
        $result = Subscribe::where('blogid', '=', $blogid)
            ->where('userid', '=', Auth::user()->id)
            ->first();
        if($result == null) {
            return false;
        }
        return true;
    }
}