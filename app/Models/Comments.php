<?php
namespace App\Models;
use DB;
use Auth;
use App\Models\Toolbox;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\User;

class Comments extends Model
{
    protected $table = 'comments';
    protected $appends = array('owner');
    public $timestamps = false;
    
    public static function getComments($blogPostId) {
        $comments = Comments::where('blogpostid','=', $blogPostId)->get();
        foreach($comments as $comment) {
            $comment->owner = User::find($comment->userid)->name;
            
        }
        return $comments;
    }
}