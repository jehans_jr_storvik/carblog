<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Usertags extends Model
{
    protected $table = 'usertags';
    public $timestamps = false;
    
    public static function getAllTagsForBlog($blogid) {
        return DB::table('usertags')
                ->join('tags', 'usertags.tagid', '=', 'tags.id')
                ->select('tags.tag')
                ->where('usertags.blogid', '=', $blogid)
                ->get();
    }
    
    public static function insertUsertag($tag, $blogid) {
        $tagId = Tags::getIdForTag($tag);
        $exists = Usertags::where('tagid', '=', $tagId)
                ->where('blogid', '=', $blogid)
                ->first();
        if($exists == null) 
        {
            $usertag = New Usertags;
            $usertag->tagid = $tagId;
            $usertag->blogid = $blogid;
            $usertag->save();
        }
    }
}