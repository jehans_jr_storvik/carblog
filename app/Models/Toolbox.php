<?php
namespace App\Models;
use DB;
use Auth;
use App\Models\Toolbox;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Toolbox extends Model
{
    protected $table = 'blog';
    public $timestamps = false;
    
    public static function getCorrectItem(Request $request) {
        //Gets the "latest" correct id of the url
        $item;
        $inc = $request->url();
        if (strpos($inc,'/editBlog/') !== false) {
            $split = "/editBlog/";
        }
        
        $inc = str_replace("http://", "", $inc);
        $inc = str_replace("localhost/", "", $inc);
        $inc = str_replace("virtualgarage.com/", "", $inc);
        $inc = str_replace("10.0.0.76/", "", $inc);
        $inc = str_replace("/editBlog", "", $inc);
        
        $url = explode("/", $inc);
        $userName = $url[0];
        $blogUrl;
        
        
        switch(count($url))
        {
            case(3):
                $blogUrl = $url[1];
                $blogPostTitle = urldecode($url[2]);
                $item = DB::table('blogposts')
                    ->join('blog', 'blogposts.blogid', '=', 'blog.id')
                    ->join('users', 'blog.user', '=', 'users.id')
                    ->select('blogposts.*')
                    ->where('users.name', '=', $userName)
                    ->where('blog.url', '=', $blogUrl)
                    ->where('blogposts.title', '=',$blogPostTitle)
                    ->get();
                break;
                //Returns a blog
                //user/blog
            case(2):
                $blogUrl = $url[1];
                $item = DB::table('blog')
                    ->join('users', 'blog.user', '=', 'users.id')
                    ->select('blog.*')
                    ->where('users.name', '=', $userName)
                    ->where('blog.url', '=', $blogUrl)
                    ->get();
                break;
            case(1):
                 $item = DB::table('users')
                    ->select('users.*')
                    ->where('users.name', '=', $userName)
                    ->get();
                break;
        }
        
        return $item[0]; 
        
        
    }
}