<?php
namespace App\Models;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $appends = array('completeUrl, owner');
    protected $table = 'blogposts';
    public $timestamps = false;
    
    
    public static function createNewBlogPost($request) {
        $url = $request->url();
        $url = explode("/", $url);
        $blogUrl = $url[count($url)-2];
        
        $content = str_replace('style="padding:10px;" width="35%"', '', $request->input('content'));
        
        DB::table('blogposts')->insertGetId([
                    'title' => $request->input('title'),
                    'content' => $content,
                    'blogid' => Blog::getBlogId($blogUrl, Auth::user()->id),
                ]);
        
        
            
        return 1;
    }

    public static function getOwner($blogid) {
        return BlogPost::join('blog','blogposts.blogid', '=', 'blog.id')
                ->join('users','blog.user','=','users.id')
                ->where('blogposts.id','=', $blogid)
                ->first()->name;
        
                
    }
    public static function addViewCount($id) {
        $blogPost = BlogPost::getBlogPost($id);
        Blog::addViewCount($blogPost[0]->blogid);
        DB::table('blogposts')
                ->where('id', $id)
                ->increment('views');
    }
    
    public static function getBlogPost($id) {
        $data = DB::table('blogposts')
                ->select()
                ->where('id', '=', $id)
                ->get();
        return $data;
    }
    public static function getLatestBlogPosts() {
        $data = DB::table('blogposts')
                ->select()
                ->take(3)
                ->orderBy('id', 'desc')
                ->get();
        return $data;
    }
    
    public static function generateUrlFromId($blogPostId) {
        $data = DB::table('blogposts')
                ->join('blog', 'blog.id', '=', 'blogposts.blogid')
                ->join('users', 'blog.user', '=', 'users.id')
                ->where('blogposts.id', '=', $blogPostId)
                ->get();
        $url = "/" . $data[0]->name . "/" . $data[0]->url . "/" . $data[0]->title;
        return $url;
    }
}
