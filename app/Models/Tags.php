<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'tags';
    public $timestamps = false;
    
    public static function getIdForTag($tag) {
        $result = Tags::select('id')
                ->where('tag', '=', $tag)->first();
        if($result != null) {
            return $result->id;
        }
        else {
            return Tags::insertGetId([
                    'tag' => $tag
                    ]);
        }
    }
}