    <div id="header">
        <a href="/"><h1><b>Virtualgarage</b></h1></a> 
   </div>

    <nav id="navbar-links" class="navbar navbar-inverse ">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="true">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
        
        <div id="mobileMenu">
            <ul class="nav navbar-nav ">
                    @if(Auth::check())
                    <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Your blogs<b class="caret"></b></a>
                            <ul id="ownedBlogs2" class="dropdown-menu">
                            </ul>
                        </li>
                    @else
                    <li><a href="/auth/register">Create user</a></li>
                    @endif
             </ul>   
        </div>
        
    <div id="bs-example-navbar-collapse-1" class ="collapse navbar-collapse" >
        <ul id="desktopMenu" class="nav navbar-nav ">
                    @if(!Auth::check())
                    <li><a href="/auth/register">Create user</a></li>
                    @endif
        </ul>   
        
                    @if(Auth::check())
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="/newBlog">Create a new carblog</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown" id="ownedBlogsDropdwn">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Your blogs<b class="caret"></b></a>
                            <ul id="ownedBlogs" class="dropdown-menu">
                            </ul>
                    </li>
                        
                        <li><a href="/{{Auth::user()->name}}">Profile</a></li>
                        <li><a href="/auth/logout">Logout</a></li>
                        
                    </ul> 
                    @else
                    <ul class ="nav navbar-nav navbar-right">
                        <li>
                            <form method="POST" action="/auth/login" class="navbar-form">
                                {!! csrf_field() !!}

                                    <input id="email" type="email" name="email"  class="form-control" placeholder="Email">

                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">

                                <button id="loginBtn" type="submit" class="btn btn-default">Log in</button>
                                
                            </form>
                        </li>
                    </ul>
                    @endif
                     
                    
                    
    </div>
    </nav>


