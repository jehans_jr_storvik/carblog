@extends('layouts.default')
<head>
    <title>Project blogpost {{ $blogpost->title}}</title>
</head>
@section('content')
<div id="blogpost">
<h1>{{ $blogpost->title}}</h1>
<h4>{{$blogpost->datecreated }}</h4>
<p>{!!  $blogpost->content !!}</p>
</div>
@if(Auth::check())
<form action={!! $url !!} method="post" >
    <textarea id="comment"   name="comment" rows="4" cols="40" autocomplete="off"></textarea>
    </br>
    <input type="submit" class="btn btn-primary btn-sm" value="Add comment">
</form>
@endif

@foreach($comments as $comment)
<div id="comment">
    Written by: <a href="/{!! $comment->owner !!} ">{!! $comment->owner !!}</a>
<p>{!! $comment->text !!}</p>
</div>
@endforeach
@stop