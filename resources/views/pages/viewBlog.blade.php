@extends('layouts.default')
<head>
    <title>Carblog project {{$blog->url}}</title>
</head>
@section('content')
<script src="/js/followBlog.js" ></script>

@if(!isset($blogPosts[0]->title))
<h2>No blogposts yet</h2>
@endif
@if($edit == true)
<div>
    <a class ="btn btn-primary btn-lg" href="{{ $url }}/newBlogPost">Add new post</br>
    </a>

    <a class ="btn btn-primary btn-lg" href="{{ $url }}/editBlog">Edit blog</br>
    </a>
</div>
@endif
@if(Auth::check() && $subscribes == false && Auth::user()->id != $blog->user)
<div>
    <a id='follow' class="btn btn-primary btn-lg" onclick='subscribe({{ $blog->id }})'>Follow blog</br>
    </a>
</div>
@elseif($subscribes == true)
    <div>
        <a id='follow' class ="btn btn-primary btn-lg" onclick='subscribe({{ $blog->id }})'>Unfollow</br>
        </a>
    </div>
@endif
<div id="blogInfo">
    <div id="viewBlogLogo">
        <img class="img-responsive" src="{{$blog->logo}}">
        </div>
    <div id="blogText">
        <p>
            <b>Car make: </b><span class="label label-primary">{{$make}}</span></br>
            <b>Car model: </b><span class="label label-primary">{{$model}}</span></br>
            <b>Engine: </b><span class="label label-primary">{{$blog->engine}}</span></br>
            <b>Year: </b> <span class="label label-primary">{{$blog->year}}</span></br>
            <b>About:</b>{{$blog->about}}<br/>
            <b>Tags: </b> 
            @foreach($tags as $tag)
                <span class="label label-success">{{$tag->tag}}</span>
            @endforeach
            </br>
        </p>
    </div>

</div>

@foreach($blogPosts as $blogPost)
<div id = "blogpost">
    <h2><a id="blogpostTitle" href="{{ $url }}/{{$blogPost->title}}">{{$blogPost->title}}</a></h2>
    <h4>{{  $blogPost->datecreated }}</h4>
    <p>{!!$blogPost->content!!}</p>
</div>
<div id="brder"></div>
@endforeach
@stop