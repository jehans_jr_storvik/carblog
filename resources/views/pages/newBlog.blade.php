@extends('layouts.default')
<head>
    <title>New carblog project</title>
</head>
@section('content')
<div id="wrapper">
<script src="/js/newBlog.js"></script>
    <h1>Create new blog</h1>
    <form method="POST" action="/newBlog" class="form-horizontal" id="registrationForm" autocomplete="off">
    {!! csrf_field() !!}
    <div class="form-group" id="url">
        <input id="urlTxtField" 
               autocomplete="off"  
               type="text" 
               class="form-control" 
               name='url' 
               placeholder="URL"  
               required title="
               Allowed : A-Z, 0-9, and '_'
               3-30 characters.
               Allowed : 
               BMW_E36_Turbo
               Not Allowed:
               BMW_E36_Turbo**#"
               pattern="[A-Za-z0-9_]{3,30}"
               
               title="Only A-z 0-9">
        <div id="completedUrl" class="h4 form-control" ><b>
                virtualgarage.com/{{$url}}/<span id="userUrlInput" ></span></b>
        </div>
    </div>

    
    <p></p>
    <div class="form-group">
    {!! Form::select('carmakes_makes_id', $carmakes, null, ['class' => 'form-control', 'id' => 'carmakes']) !!}
    </div>
    <p></p>
    <div class="form-group">
    {!! Form::select('carmodels_models_id', $carmodels, null, ['class' => 'form-control', 'id' => 'carmodels']) !!}
    </div>
    <p></p>

    <div class="form-group">
        <input id="caryear" 
               class="form-control" 
               name='Year'
               maxlength="4"
               min="1800" 
               max="2019"
               title="The year of the car"
               placeholder="Year of car" 
               required title="Year YYYY(Ex. 1992)" 
               type="number" 
               >
    </div>
    
    <p></p>
    <div class="form-group">
    <button type="submit" class="btn btn-primary">Create new blog</button>
    </div>
    </form> 
   <ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
@stop
</div>