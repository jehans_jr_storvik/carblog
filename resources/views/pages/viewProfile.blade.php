@extends('layouts.default')
<head>
    <title>Profile for {{$user->name}}</title>
</head>
@section('content')


<h1>Profile for {{ $user->name }}</h1>
<div id="profile">
    
    @if($edit == true)
        
    
    <form class="form-horizontal" method="POST" action="">
        {!! csrf_field() !!}

        <div class="col-md-offset-3">
            <div class="form-group ">
              <label for="email" class="col-sm-2 control-label">Email address:</label>
                  <div class="col-sm-5">
                      <input type="email" class="form-control" id="email" name="email" value={!! $user->email !!} disabled>
                  </div>
            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Username:</label>
              <div class="col-sm-5">
                  <input type="username" class="form-control" id="username"  value={!! $user->name !!} disabled>
              </div>
            </div>

            <div class="form-group">
              <label for="current_password" class="col-sm-2 control-label">Current password:</label>
              <div class="col-sm-5">
                  <input type="password" class="form-control" id="current_password" name="current_password">
              </div>
            </div>
            
            <div class="form-group">
              <label for="new_password" class="col-sm-2 control-label">New password:</label>
              <div class="col-sm-5">
                  <input type="password" class="form-control" id="new_password" name="new_password" pattern=".{6,}" required title="Minimum 6 characters in password">
              </div>
            </div>

              <div class="form-group">
                  <label for="password_confirmation" class="col-sm-2 control-label">Confirm new password:</label>
                  <div class="col-sm-5">
                      <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" pattern=".{6,}" required title="Minimum 6 characters in password">
                  </div>
              </div>
        </div>
        <ul>
            @foreach ($errors as $error)
            <b><li>{{ $error }}</li></b>
            @endforeach
        </ul>
        <div>
            <button type="submit" class="btn btn-default">Change password</button>
        </div>
    </form>
    @endif
    
    <h3>Subscribed blogs</h3>
    <p>Coming....</p>
    <label for="basic-url">  List of blogs  - </label>
    @foreach($blogs as $blog)
        <h4><a href="{{$blog->completeUrl}}">{{$blog->url}}</a></h4>
    @endforeach
</div>

@stop

