@extends('layouts.default')
<head>
    <title>View profile for {{$user->name}}</title>
</head>
@section('content')

<div class="form-inline">
    <h1>Viewing profile for {{$user->name}}</h1>
    
    @foreach($blogs as $blog)
    <h4><a href="{{$blog->completeUrl}}">{{$blog->url}}</a></h4>
    @endforeach
</div>

@stop

