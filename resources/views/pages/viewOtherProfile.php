@extends('layouts.default')

@section('content')


<h1>Edit your profile</h1>
<div id="profile" class="form-inline">

    <label for="basic-url">Username -</label>
    <div class="input-group ">
      <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" value={!! $user->name !!} disabled>
    </div>
    </br>
      
    <label for="basic-url">  Email  -</label>
    <div class="input-group">
      <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" value={!! $user->email !!} disabled>
    </div>
    </br>
    
    
    
    </br>
    <label for="basic-url" >  About - </label>
    <div class="input-group">
      <input type="textarea" class="form-control" id="basic-url" aria-describedby="basic-addon3" value={!! $user->about !!} >
    </div>
    </br>
    </br>
    <label for="basic-url">  List of blogs  - </label>
    @foreach($blogs as $blog)
    <h4><a href="{{$blog->completeUrl}}">{{$blog->url}}</a></h4>
    @endforeach
</div>

@stop

