@extends('layouts.default')
<head>
    <title>New project blogpost</title>
</head>
@section('content')
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/plupload/js/moxie.js"></script>
<script type="text/javascript" src="/plupload/js/plupload.dev.js"></script>
<script src="/js/newBlogPost.js"></script>
<div id='newBlogPostWrapper'>
    {!! Form::open(array('url' => Request::url(),  'name' => 'newBlogPost','method' => 'post', 'files' => true)) !!}

    <div class="form-group">
    <input name ="title" 
           type="text" 
           id="titleNewBlogPost" 
           class="form-control" 
           placeholder="Title" 
           required 
           title="
               Allowed characters:
               A-z, 0-9, ?, _
               3-100 characters
               "
               pattern="[A-Za-z0-9_\s\?,+]{3,100}"
           >
    </div>

    <div id="container">
        <a class ="btn btn-primary btn-sm pull-right" id="pickfiles"  href="javascript:;">Select files</a> 
        <a class ="btn btn-primary btn-sm " id="uploadfiles"  href="javascript:;">Upload files</a>
    </div>

        
    </br>
    </br>
    <div id ='ckeditorArea'>
    {!! Form::textarea('content', null, ['class' => 'form-control', 'id' => 'blogPostContent']) !!}
    </div>
    <div id="uploadedPhotos">    
        <input type="button" class="btn btn-info" id="addAllPhotos" value="Add all">
    </div>
    <script>
                CKEDITOR.config.allowedContent =
    'h1 h2 h3 p blockquote strong em;' +
    'a[!href];' +
    'img(left,right)[!src,alt,width,height];'; 
                CKEDITOR.replace('blogPostContent');
                
    </script>

    <div id="tepper">
        <input type="submit" value="Submit" class="btn btn-primary pull-right" id='submitBlogPost'>
    </div>
    {!! Form::close() !!}
</div>
    <br />
    
    </br>
    <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
<script type="text/javascript">
var uploader = new plupload.Uploader({
	runtimes : 'html5,flash,silverlight,html4',
        unique_names: true,
	browse_button : 'pickfiles', // you can pass an id...
	container: document.getElementById('container'), // ... or DOM Element itself
	url : '/uploadPhotos2',
        resize: {
            width:1900,
            height:1900,
        },
        
	flash_swf_url : '/plupload/js/Moxie.swf',
	silverlight_xap_url : '/plupload/js/Moxie.xap',
	
        
	filters : {
		max_file_size : '10mb',
		mime_types: [
			{title : "Image files", extensions : "jpg,gif,png"},
		]
	},

	init: {
		PostInit: function() {
			document.getElementById('filelist').innerHTML = '';

			document.getElementById('uploadfiles').onclick = function() {
				uploader.start();
				return false;
			};
		},

		FilesAdded: function(up, files) {
                    up.start();
			plupload.each(files, function(file) {
				document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
			});
		},

		UploadProgress: function(up, file) {
			document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
		},
                FileUploaded: function(up, file) {
                    var path = "/uploads/" + file.target_name;
                    //var imgDiv = "<div id='phtUpload' onClick=usePhotoInBlogPost('"+ path +"')><img src='" + path + "'/>";
                    var imgDiv = "<div id='phtUpload' onClick=usePhotoInBlogPost('"+ path +"')><div id='phtUploadDiv'><img id='" + path + "' src='" + path + "'/></div>";
                
                    $('#uploadedPhotos').show();
                    $('#uploadedPhotos').append(imgDiv);
                    
                    $('#addAllPhotos').show();
                },
		Error: function(up, err) {
			document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
		}
	}
});

uploader.init();
uploader.refresh();
</script>
@stop
