@extends('layouts.default')
<head>
    <title>Edit your carblog project</title>
</head>
@section('content')
<div id="wrapper">
<script type="text/javascript" src="/plupload/js/moxie.js"></script>
<script type="text/javascript" src="/plupload/js/plupload.dev.js"></script>
<script src="/js/newBlog.js"></script>
<script src="/js/editBlog.js"></script>
    <h1>Edit your blogdetails</h1>
    @if($success != "")
    <div class="alert alert-success form-group" role="alert">
        <a href="#" class="alert-link">Blog successfully updated!</a>
    </div>
    @endif
    <form method="POST" action={!! Request::url() !!} class="form-horizontal" id="registrationForm" autocomplete="off">
    {!! csrf_field() !!}
    <p></p>
    <div class="form-group">
    {!! Form::select('carmakes_makes_id', $carmakes, null, ['class' => 'form-control', 'id' => 'carmakes', 'default' => $blog->carmakeid]) !!}
    </div>
    <p></p>
    <div class="form-group">
    {!! Form::select('carmodels_models_id', $carmodels, null, ['class' => 'form-control', 'id' => 'carmodels', 'default' => $blog->carmodelid]) !!}
    </div>
    <p></p>

    
    <label for="caryear">Production year of car</label>
    <div class="form-group">
        <input id="caryear" 
               class="form-control" 
               name='Year'
               maxlength="4"
               min="1800" 
               max="2019"
               placeholder="Year of car" 
               required title="Year YYYY(Ex. 1992)" 
               type="number"
               value={!! $blog->year !!}
               >
    </div>
    
    <div class="form-group">
        <label for="tags">Tags that describe your blog. Example "Tuned, Custom, Turbo"</label>
        <input name="tags" type="text" class="form-control" value="{{$tags}}">
    </div>
    
    
    <label for="about">About this project</label>
    <div class="form-group">
          <textarea name="about" class="form-control" rows="5" id="about" maxlength="1000" >{{ $blog->about }}</textarea>
    </div>
    
    <label for="about">Engine</label>
    <div class="form-group">
          <input type="text" name="engine" class="form-control"  id="about" maxlength="50" value="{{ $blog->engine }}">
    </div>
    
    
    <img id='logo' src={!! $blog->logo !!} >
    <input id="invislogo" name="logo" type="hidden" value={!! $blog->logo !!} >
    
    <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
    <br />

    
<div id="container2">
        <a id="pickfiles" href="javascript:;">[Select new logo]</a> 
        <a id="uploadfiles" href="javascript:;">[Upload logo]</a>
</div>
    <p>The logo will not be changed until "Save changes" has been clicked.</p>
    <ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul> 
    <br />
    <p></p>
    <div class="form-group">
    <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
    </form>
     
    

<script type="text/javascript">
var uploader = new plupload.Uploader({
	runtimes : 'html5,flash,silverlight,html4',
        unique_names: true,
	browse_button : 'pickfiles', // you can pass an id...
	container: document.getElementById('container2'), // ... or DOM Element itself
	url : '/uploadPhotos2',
        resize: {
            width: 425,
            height:425
            
          },
	flash_swf_url : '/plupload/js/Moxie.swf',
	silverlight_xap_url : '/plupload/js/Moxie.xap',
	
	filters : {
		max_file_size : '10mb',
		mime_types: [
			{title : "Image files", extensions : "jpg,gif,png"}
		]
	},

	init: {
		PostInit: function() {
			document.getElementById('filelist').innerHTML = '';

			document.getElementById('uploadfiles').onclick = function() {
				uploader.start();
				return false;
			};
		},

		FilesAdded: function(up, files) {
                    up.start();
			plupload.each(files, function(file) {
				document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
			});
		},

		UploadProgress: function(up, file) {
			document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
		},
                FileUploaded: function(up, file) {
                    console.log(file);
                    console.log(up);
                    $('#logo').attr("src","/uploads/" + file.target_name);
                    $('#invislogo').val("/uploads/" + file.target_name)
                    
                },
		Error: function(up, err) {
			document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
		}
	}
});

uploader.init();


</script>
@stop
</div>
