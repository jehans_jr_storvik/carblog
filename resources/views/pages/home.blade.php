@extends('layouts.default')
<head>
    <title>Home</title>
</head>
@section('content')
<div id="main">
    <h1>Latest project blogposts</h1>
    @foreach($blogposts as $blogpost)
    <div id="mainContent">
        <h2><a href="{{$blogpost->completeUrl}}">{{$blogpost->title}}
            </a></h2>
        <p>Written by:<b> <a href="/{{$blogpost->owner}}">{{$blogpost->owner}}</a></b><br/>
            Date written: <b>{{$blogpost->datecreated}} </b></p>
        
        {!! $blogpost->content !!}
    </div>
    @endforeach
</div>

<div id="sidebar">
        <h3>Most viewed</h3>
        <div class="form-group" id="topten">
            <input type="text" class="form-control" placeholder="Search" id="search">
            <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
            {!! Form::select('carmakes_makes_id', $carmakes, null, ['class' => 'form-control', 'id' => 'carmakes']) !!}
        </div>    
        <div id="sidebarList">
        @foreach($blogs as $blog)
        <h4><a href="{{$blog->completeUrl}}">{{$blog->url}} </br>
           <img src="{{$blog->logo}}"></img></a></h4>
        @endforeach
        </div>
</div>
<script src="/js/topten.js" async="true"></script>
@stop
