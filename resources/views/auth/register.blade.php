@extends('layouts.default')
@section('content')
<!-- resources/views/auth/register.blade.php -->
<script src="/js/newUser.js"></script>
<div id="wrapper">
<form method="POST" action="/auth/register" class="form-horizontal" id="registrationForm">
    {!! csrf_field() !!}
    <h1>Register new user</h1>
    <div class="form-group">
        <input type="text" id="name" class="form-control"
               name="name" value="{{ old('name') }}" 
               placeholder="Username" 
               required title="Allowed : A-Z, 0-9, and '_'
               3-30 characters.
               Allowed : 
               Josh_Nor
               Not Allowed:
               Josh**#"
               pattern="[A-Za-z0-9_]{3,30}"
               autocomplete="off">
    </div>
    <h4 id="nameExists"><b>That username already exists</b></h4>

    <div class="form-group">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
    </div>

    <div class="form-group">
        <input id="password-form" 
               type="password" 
               class="form-control" 
               name="password" 
               placeholder="Password" 
               required
               pattern=".{6,}" 
               title="Six or more characters">
    </div>

    <div class="form-group">
        <input id="confirm-password" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>
    </div>
    
    <h4 id="helper"><b>Passwords do not match</b></h4>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div>
        <input id="submitNewUser" type="submit" class="btn btn-primary" disabled>
    </div>
</form>
@stop
</div>
