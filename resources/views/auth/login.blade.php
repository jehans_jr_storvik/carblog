@extends('layouts.default')
@section('content')
<div class="col-md-6 col-md-offset-3">
<form method="POST" action="/auth/login">
    {!! csrf_field() !!}

    <div >
        
        <input type="email" name="email" placeholder="Email" value="{{ old('email') }}" class="form-control">
    </div>

    <div >
        
        <input placeholder="Password" type="password" name="password" class="form-control">
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>
    
    <div>
        <button class="btn btn-primary" type="submit">Login</button>
    </div>
</form>
    </br>
    <a href='/password/email'>Click here to reset your password</a>

</div>

@stop