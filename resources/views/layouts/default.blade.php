<!doctype html>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<meta charset="utf-8">
<html>
<head>
<style>

#comment {
            padding:10px;
            border: 2px solid black;
            width: 25%;
            margin:auto;
            margin-top:10px;
        }
#header {
    color:white;
    font-size:large;
    background:rgba(16, 16, 16,0.5);
    margin-top:-10px;
    text-align: center;
}
body {
        min-width: 250px;
        background: url('/images/bgimage3.jpg')no-repeat center center fixed; 
        
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    }
#header a {
    color:white;
}
#header h1 {
    margin-left:20px;
    padding-top:5px;   
}

#sidebar img {
    max-width:95%;
}
#sidebar {
    border: 3px solid black;
    float:top;
    margin-bottom: 5px;
    margin-left: 67%;
    width:32%;
}
#uploadedPhotos {
    overflow: scroll;
    border: 2px solid black;
    width:29%;
    height:26.5em;
    float:right;
}

#tepper {
    width:100%;
}
.custom {
    width: 8.5em !important;
}
#phtUploadDiv img{
    box-sizing: border-box;
    width: 100px;
    height:100px;
    float:left;
    margin:2px;
}
#phtUploadDiv img .border {
    height:55px;
}
#fileUploader {
    text-align:center;
}
#fileUploader #chooseFiles{
    float:left;
}

#uploadfiles {
    display:none;
}
#titleNewBlogPost {
    float:left;
    width:70%;
}
#loginBtn{
    width:10em;
    margin-right:0px;
}
#buttonHome {
    width:15em;
    height:2.5em;
}
#uploadBtn {
    float:right;
}
#chooseFiles{
    min-width:220px;
    max-width:230px;
}
#mainContent {
    float:left;
    width:65%;
    margin:auto;
    border: 3px solid black;
    display: inline-block;
}

#imgDiv {
    width:auto;
}
#mainContent img {
display: block;
    max-width:95%;
    padding-bottom: 5px;
    width: auto;
    height: auto;
    margin:auto;
}
#url {
    min-width:200px;
}
#completedUrl {
    display: none;
    overflow-x:hidden;
    direction:rtl;
    min-height:35px;
}
#userUrlInput {
    overflow-x:scroll;
}
#blogPostContent {
    float:left;
}
#blogPostContent img {
    padding:10px;
    width:100%;
}
#blogpostTitle {
    color: black;
}
#blogpost {
    border: 3px solid #ddd;
    padding: 10px; 
    margin-top: 10px;
    width: 100%;
}
#blogpost img {
    padding:10px;
    max-width:98%;
    max-height: 75vh;
    height:auto;
}
.form-group {
    text-align: center;
}
.form-control {
    margin:auto;
    width: 50%;
}
.link {
    color:blue;
    text-decoration:underline;
    cursor:pointer;
}
.row {
    height: 15%;
}
#navbar-links li a{
   font-size: 1.8em;
   font-weight: bold;
   color:white !important;
}
#ownedBlogs {
    background: #333;
}
#footer {
    bottom: 0;
     background: rgba(0,0,0,0.5);
    height: 2em;
    left: 0;
    position: fixed; /* OldProperty */
    position: static;/* Updated Property */
    font-size: small;
    width:100%;
}
#brder {
    border: 4px solid #000;
    margin-left:-1%;
    width:102%;
}
#registrationForm .form-control {
    width:75%;
    min-width:220px;
}
p.navbar-text {  
  font-size: 22px;
  margin-top: 8px;
  padding-bottom: 0;
  text-align: right;
  width: 70%;
}
.navbar {
    margin-top:10px;
    margin-bottom:0px !important;
}
#wrapper {
    min-height:45em;
    display: inline;
    background-color:#f1f1f1;
    width: 100%;
    padding-left: 15px;
    padding-right:15px;
    text-align: center;
    float:left;
}
#content {
    min-width:250px;
    opacity: 1;
    width: 90%;
    text-align: center;
    margin:0 auto;
}
#blogInfo {
    display: inline-block; 
}
#viewBlogLogo {
    float:left;
    padding-top: 10px;
    width: 49%;
}
#blogText {
    width: 49%;
    float:left;
    padding-left: 10px;
    padding-top: 10px;
    text-align: left;
}
#addAllPhotos {
    margin:auto;
    float:left;
    width:100%;
    display:none;
}
#container {
    width: 18em;
    float:right;
}
#ckeditorArea {
    width: 70% !important;
    float:left;
}
#submitBlogPost {
    margin-top: 10px;
}
#filelist {
    
}
body.modal-open {
    
}
#helper {
    display: none;
}
#nameExists {
    display: none;
}
#text {
  z-index: 100;
  position: absolute;
  color: white;
  font-size: 24px;
  font-weight: bold;
  left: 150px;
  top: 350px;
}
</style>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script


    @include('includes.head')
</head>
<body>
<div id="content">
        @include('includes.header')
    <div id = "wrapper">
        <div id="routeHelper"></div>
            @yield('content')
    </div>

        @include('includes.footer')
        
</div>
</body>
    <script src="/js/getOwnedBlogs.js" async="true"></script>
    <script src="/js/routeHelper.js" async="true"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" async="true"></script>
    <script src="/js/lightbox/dist/ekko-lightbox.js" async="true"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" async="true">
    <link rel="stylesheet" href="/css/screenSizeRelevant.css" async="true">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine" async="true">
    <link href="/js/lightbox/dist/ekko-lightbox.css" rel="stylesheet" async="true">
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-71686194-1', 'auto');
    ga('send', 'pageview');

  </script>
</html>
